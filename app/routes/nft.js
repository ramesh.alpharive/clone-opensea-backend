const express = require('express')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

const { roleAuthorization } = require('../controllers/auth')

const {
  getOneNft,
  createNft,
  getNft,
  activity
  } = require('../controllers/nft')

const {
  validateCreateNft,

} = require('../controllers/nft/validator/validateCreateNft')


router.post(
  '/create_nft',
    trimRequest.all,
    requireAuth,
    roleAuthorization(['user']),
    validateCreateNft,
    createNft
)

router.post(
  '/get_nft',
    requireAuth,
    roleAuthorization(['user']),
    trimRequest.all,
    getNft
)

router.post(
  '/get_my_nft',
    requireAuth,
    roleAuthorization(['user']),
    trimRequest.all,
    getOneNft
)
router.post(
  '/nft_activity',
    // requireAuth,
    // roleAuthorization(['user']),
    trimRequest.all,
    activity
)


module.exports = router
