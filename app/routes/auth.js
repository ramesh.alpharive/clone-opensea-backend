const express = require('express')
const multer = require('multer')
const router = express.Router()
require('../../config/passport')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {
  session: false
})
const trimRequest = require('trim-request')

const {
  register,
  verify,
  forgotPassword,
  resetPassword,
  getRefreshToken,
  createCollection,
  loginByAdmin,
  roleAuthorization,
  getCollection,
  getallCollection,
  createUser,
  uploadImage,
  getNftByCollectionid,
  getMyCollection,
  getCollectionDetails
  
} = require('../controllers/auth')

const {
  validateRegister,
  validateVerify,
  validateForgotPassword,
  validateResetPassword,
  validateCreateCollection,
  validateCreateUser,
  validategetCollectionDetails
} = require('../controllers/auth/validators')



const imageupload = multer({
  storage: multer.diskStorage({
    destination: './public/images',
    filename(req, res, cb) {
      cb(null, `${Date.now()}_${res.originalname}`)
    }
  })
})


/*
 * Auth routes
 */

/*
 * Register route
 */
router.post('/register', trimRequest.all, validateRegister, register)

/*
 * Verify route
 */
router.post('/verify', trimRequest.all, validateVerify, verify)

/*
 * Forgot password route
 */
router.post('/forgot', trimRequest.all, validateForgotPassword, forgotPassword)

/*
 * Reset password route
 */
router.post('/reset', trimRequest.all, validateResetPassword, resetPassword)

/*
 * Get new refresh token
 */
router.get(
  '/token',
  requireAuth,
  roleAuthorization(['user', 'admin']),
  trimRequest.all,
  getRefreshToken
)

/*
 * Login route
 */

router.post('/adminlogin', trimRequest.all, loginByAdmin)


// collection routes
router.post('/createcollection', trimRequest.all, validateCreateCollection, createCollection)

router.post('/getcollection', trimRequest.all, requireAuth, roleAuthorization(['user']),  getCollection)

router.post('/getnft_bycollectionid', trimRequest.all, requireAuth, roleAuthorization(['user']), getNftByCollectionid)

router.post('/get_all_collection', requireAuth, roleAuthorization(['user']), trimRequest.all, getallCollection)

router.post('/create_user', trimRequest.all, validateCreateUser, createUser)

router.post('/upload_image', trimRequest.all, imageupload.single('image'), uploadImage)

router.post('/get_my_collection', requireAuth, roleAuthorization(['user']), getMyCollection)

router.post('/get_collection_details', trimRequest.all, validategetCollectionDetails, getCollectionDetails )


module.exports = router
