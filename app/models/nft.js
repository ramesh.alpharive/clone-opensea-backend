const { Mongoose } = require("mongoose");
const mongoose = require("mongoose");
// const autoIncrement = require('mongoose-auto-increment');
const nftSchema = new mongoose.Schema(
  {
    Description: { type: String },
    Name: { type: String },
    ProductImage: { type: String },
    price: { type: Number,default:0 },
    Supply: { type: Number, },
    ExternalLink: { type: String },
    mintTransaction:{type:Object},
    ownersList: { type:Array },
   creator: { type: mongoose.Schema.Types.ObjectId, require: true, ref: 'User' },
  currentOwner: { type: mongoose.Schema.Types.ObjectId, require: true, ref: 'User' },
     activity: { type: Array },
     imageUrl: { type: String },
     contractAddress: { type: String },
     contractType : { type: String,default:"721" },
     islisted: { type: Boolean, default: false },
     ishidden: { type: Boolean, default: false },
     viewCount: { type: Number, default:0},
     likeCount: { type: Number, default:0},
     symbol: { type: String },
     auction: { type: Boolean, default: false },
     isblocked: { type: Boolean, default: false },
     network: { type: String },
     itemid:{ type: String },
     collection_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Collection' },

    // status: { type: String },
    // category: { type: String },
    // subCategory: { type: String },
    // categories: { type: String },
    // royaltyFees: { type: String },
    // isSold: { type: Boolean, default: false },
    // isReceived: { type: Boolean, default: false },
    // mintId: { type: Number, default: 1 },
    // view: { type: Array },
    // favourite: { type: Array },
    // openBid: { type: Boolean },
    // created: { type: Date, default: Date.now },
    // startDate: { type: Date },
    // isVerified: { type: Boolean, default: false },
    // isActive: { type: Boolean, default: true },
    // isDeleted: { type: Boolean, default: false },
  },
  { timestamps: true }
);
// nftSchema.plugin(autoIncrement.plugin, { model: 'nfts', field: 'mintId', startAt: 1, incrementBy: 1 });
const nftModel = mongoose.model('nfts', nftSchema);
module.exports = nftModel;