const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const validator = require('validator')
const mongoosePaginate = require('mongoose-paginate-v2')

const UserSchema = new mongoose.Schema(
  {
    Name: {
      type: String,
      required: true
  },
  Wallet_Address: {
      type: String,
      required: true
  },
  Image: {
      type: String,
      required: true
  },
  collections :{
    type: Array
  },
  role: {
    type: String,
    enum: ['user', 'Admin'],
    default: 'user'
  }
  },
  {
    versionKey: false,
    timestamps: true
  }
)


UserSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('User', UserSchema)
