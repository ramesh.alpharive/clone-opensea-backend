const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const FileSchema = new mongoose.Schema(
  {
    File_Name: {
      type: String,
      required: true
   }

  },
  {
    versionKey: false,
    timestamps: true
  }
)


FileSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('File', FileSchema)
