const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const CollectionSchema = new mongoose.Schema(
  {
    Collection_Name: {
      type: String,
      required: true
   },
   Collection_Type: {
      type: String,
      required: true
  },
  Collection_URL: {
      type: String,
      required: true
  },
  wallet_Id : {
    type: String,
    required: true
  },
  banner_image:{
    type: String,
  },
  Network: {
      type: String,
      required: true
  },
   Twitter_Url : {
      type: String,
      required: true
  },
  Insta_Url: {
    type: String,
    required: true
  },
  Bio: {
    type: String,
    required: true
  },
  Symbol:{
    type: String,
    required: true
  },
  Chain_ID:{
    type: String,
    required: true
  },


  },
  {
    versionKey: false,
    timestamps: true
  }
)


CollectionSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Collection', CollectionSchema)
