const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const Nft = require('../../models/nft')

/**
 * Login function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getOneNft = async (req, res) => {
  try {
    const id = req.query._id
    var is_mine = "false"
    // id = req.body.id

    let NFTdetail = await Nft.findOne({_id : id}).populate("currentOwner")
  
    if(String(req.user._id) == String(NFTdetail.currentOwner._id)){
      is_mine = "true"
    }else{
      const viewcount = NFTdetail.viewCount + 1
      NFTdetail = await Nft.findOneAndUpdate({_id : id},{viewCount:viewcount})
    }
    console.log(is_mine)
    const response = await Nft.findOne({_id : id}).populate("currentOwner")
    if(response.length <= 0){
        res.status(400).json({
          success : false,
          result:  [],
          message: 'No NFT Found'
        })
    }else{
      
        res.status(200).json({
            success : true,
            result:  response,is_mine,
            message: 'NFT Found Successfully'
          })
    }
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getOneNft }
