const { validateResult } = require('../../../middleware/utils')
const { check } = require('express-validator')

/**
 * Validates forgot password request
 */
const validateCreateNft = [
 
  

    check('Name')
    .exists()
    .withMessage('Name  Missing')
    .not()
    .isEmpty()
    .withMessage('Name  Is Empty'),
    
    
    check('collection_id')
    .exists()
    .withMessage('collection_id  Missing')
    .not()
    .isEmpty()
    .withMessage('collection_id  Is Empty'),

    check('Description')
    .exists()
    .withMessage('description Missing')
    .not()
    .isEmpty()
    .withMessage('description Is Empty'),

    check('ProductImage')
    .exists()
    .withMessage('ProductImage Missing')
    .not()
    .isEmpty()
    .withMessage('ProductImage Is Empty'),

   

    check('TransactionHash')
    .exists()
    .withMessage('TransactionHash Missing')
    .not()
    .isEmpty()
    .withMessage('TransactionHash Is Empty'),

    check('TransactionFrom')
    .exists()
    .withMessage('TransactionFrom Missing')
    .not()
    .isEmpty()
    .withMessage('TransactionFrom Is Empty'),

    check('TransactionTo')
    .exists()
    .withMessage('TransactionTo Missing')
    .not()
    .isEmpty()
    .withMessage('TransactionTo Is Empty'),

    check('TransactionBlockNumber')
    .exists()
    .withMessage('TransactionBlockNumber Missing')
    .not()
    .isEmpty()
    .withMessage('TransactionBlockNumber Is Empty'),

    check('TransactionGasUsed')
    .exists()
    .withMessage('TransactionGasUsed Missing')
    .not()
    .isEmpty()
    .withMessage('TransactionGasUsed Is Empty'),

    check('TransactionTokenID')
    .exists()
    .withMessage('TransactionTokenID Missing')
    .not()
    .isEmpty()
    .withMessage('TransactionTokenID Is Empty'),

    check('Supply')
    .exists()
    .withMessage('Supply Missing')
    .not()
    .isEmpty()
    .withMessage('Supply Is Empty'),

    check('ExternalLink')
    .exists()
    .withMessage('ExternalLink Missing')
    .not()
    .isEmpty()
    .withMessage('ExternalLink Is Empty'),
  
  (req, res, next) => {
    validateResult(req, res, next)
  }
]

module.exports = { validateCreateNft }
