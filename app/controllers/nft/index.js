const { createNft } = require('./createNft')
const { getNft } = require('./getNft')
const { getOneNft } = require('./getOneNft')
const {activity } = require('./activity')


module.exports = {
  createNft,
  getNft,
  getOneNft,
  activity
}
