const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const Nft = require('../../models/nft')

/**
 * Login function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const activity = async (req, res) => {
  try {
    const id = req.body.id
    let price=req?.body?.price
    let itemid = req.body.TransactionItemID
   const obj = {
        TransactionHash : req?.body?.TransactionHash,
        TransactionFrom : req?.body?.TransactionFrom,
        TransactionTo : req?.body?.TransactionTo,
        TransactionBlockNumber : req?.body?.TransactionBlockNumber,
        TransactionGasUsed : req?.body?.TransactionGasUsed,
        TransactionItemID:req?.body?.TransactionItemID,
       
    }

    const updateCri = {
      price:price,
      itemid:itemid,
        $push : {activity : obj},
        islisted : true
    }
    const response = await Nft.findOneAndUpdate({_id : id},updateCri);

    if(response){
        res.status(200).json({
            success : true,
            result:  response,
            message: 'Nft Updated Successfully'
          })
    }else{
        res.status(400).json({
            success : false,
            result:  ' ',
            message: 'Nft Not Found'
          })
    }

  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { activity }
