const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const Nft = require("../../models/nft")

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createNft = async (req, res) => {
  try {
    
    console.log(req.user.id,'body');
    const owns = []
    owns.push(req.user.id)
    const nft = {
    
      Description : req.body.Description,
      Name :  req.body.Name, 
      collection_id:req.body.collection_id,
      ProductImage :  req.body.ProductImage, 
      price:  req.body.price,
   mintTransaction:{
    TransactionHash: req.body.TransactionHash,
    TransactionFrom : req.body.TransactionFrom,
    TransactionTo : req.body.TransactionTo,
    TransactionBlockNumber : req.body.TransactionBlockNumber,
    TransactionGasUsed :req.body.TransactionGasUsed,
    TransactionTokenID:  req.body.TransactionTokenID,
   },
   creator:req.user.id,
   currentOwner:req.user.id,
   ownersList : owns,
   Supply : req.body.Supply,
   ExternalLink : req.body.ExternalLink,
      // sellTransaction : req.body.formContent.sellTransaction,
      // auctionTransaction : req.body.formContent.auctionTransaction,
      // activity : req.body.formContent.activity,
      // isAuction : req.body.formContent.isAuction,
      // auctionMinBid : req.body.formContent.auctionMinBid,
      // auctionTime : req.body.formContent.auctionTime,
      // unblockableContent : req.body.formContent.unblockableContent,
      // sensitiveContent : req.body.formContent.sensitiveContent,
      // freezeMetadata : req.body.formContent.freezeMetadata,
      // description : req.body.formContent.description,
      // status : req.body.formContent.status,
      // category : req.body.formContent.category,
      // subCategory : req.body.formContent.subCategory,
      // categories : req.body.formContent.categories,
      // price : req.body.formContent.price,
      // royaltyFees : req.body.formContent.royaltyFees,
      // isSold : req.body.formContent.isSold,
      // isReceived : req.body.formContent.isReceived,
      // mintId : req.body.formContent.mintId,
      // view : req.body.formContent.view,
      // openBid : req.body.formContent.openBid,
      // created : req.body.formContent.created,
      // startDate : req.body.formContent.startDate,
      // isVerified : req.body.formContent.isVerified,
      // isActive : req.body.formContent.isActive,
      // isDeleted : req.body.formContent.isDeleted,
      // network : req.body.formContent.network,
      // collection_id : req.body.forNftmContent.collection_id,
      // creator : req.body.formContent.creator,
      // userId : req.body.formContent.userId

    }
    // nft.save((err) => {
    //   if (err) {
    //    //res.status(400).json(err) 
    //   console.log(err,'error saving');    
    //    }
    // })
    await Nft.create(nft)
   
      res.status(200).json({
        success:true,
        result:nft,
        message:"NFT Created Successfully"
      })
    
  } catch (error) {
    console.log(error,"errors")
    handleError(res, error)
  }
}

module.exports = { createNft }
