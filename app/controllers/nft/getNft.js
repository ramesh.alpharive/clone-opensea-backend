const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const Nft = require('../../models/nft')

/**
 * Login function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getNft = async (req, res) => {
  try {
    const id = req.query._id
    // id = req.body.id

    const response = await Nft.find({collection_id : id}).populate("currentOwner")
    
    if(response.length <= 0){
        res.status(400).json({
          success : false,
          result:  [],
          message: 'No NFT Found'
        })
    }else{
        res.status(200).json({
            success : true,
            result:  response,
            message: 'NFT Found Successfully'
          })
    }
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getNft }
