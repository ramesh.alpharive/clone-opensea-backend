const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const nft = require("../../models/nft")

/**
 * Create item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getNftByCollectionid = async (req, res) => {
  try {
    
    console.log(req.user,'usertoken');
  
    // const collections = await req.body.collection_id
  // console.log(req.body.collection_id,'qwertyhujikjhgvfdcfgh');

    const Exists = await nft.find({collection_id:req.body.collection_id})
  // console.log(req.body.collection_id,'collection');
  // console.log(Exists,'collection');

  if(Exists.length <= 0){
    res.status(400).json({
      success : false,
      result:  " ",
      message: 'NFT Not Found'
    })
  }else{
    res.status(200).json({
      success : true,
      result:  [Exists],
      message: 'NFT Found Successfully'
    })
  }

  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { getNftByCollectionid }
