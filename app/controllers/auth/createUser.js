const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const { generateToken } = require('./helpers/generateToken')
const User = require('../../models/user')

/**
 * Login function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createUser = async (req, res) => {
  try {
    // console.log(req.body,'username');
    const { Name, Wallet_Address, Image } = req.body
   // const {  Wallet_Address,NAme } = req.body

    const exists = await User.findOne({Wallet_Address : Wallet_Address})
    // console.log(exists, 'wallet_address');

    if(!exists){

    const user = new User({
      Wallet_Address,
       Name,
       Image
     })
     user.save(async(err) => {
       if (err) {
        console.log(err,"error")
        res.status(400).json({
          success : false,
          result: "" ,
          message: 'Something went Wrong'
        })
      }
      else{
        const token = await generateToken(user._id)

        res.status(200).json({
         success : true,
         result: token,
         message: 'User Created Successfully'
       })
      }
     })

    
    }else{

      const userToken = await generateToken(exists._id)
      res.status(200).json({
        success : true,
        result:  userToken,
        message: 'Login'
      })
    }

  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { createUser }
