const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const Collection = require('../../models/collections')


const getCollectionDetails = async (req, res) => {
    try {
      
      const collection_id = req.body.collection_id

      const response = await Collection.findById(collection_id)
  
      if(response){
        res.status(200).json({
            success : true,
            result:  response,
            message: 'Collection Found Successfully'
          })
      }else{
        res.status(400).json({
            success:false,
            result: '',
            message: 'Collection Not Found'
        })
      }

    } catch (error) {
      handleError(res, error)
    }
  }   


  module.exports = { getCollectionDetails }
