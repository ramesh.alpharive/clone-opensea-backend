const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const Collection = require('../../models/collections')

/**
 * Login function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const getCollection = async (req, res) => {
  try {
    
    // console.log(req.user.Wallet_Address,'wallet iis ');
console.log(req.user)
    const wallet = req.user.Wallet_Address
    // console.log(wallet,'walletsss');
  
    const response = await Collection.find({wallet_Id : { $regex: new RegExp(wallet, 'i') }})
console.log(response)
    if(response.length <= 0){
        res.status(400).json({
          success : false,
            result:  "",
            message: 'No Collection Found'}
        )
    }else{
        res.status(200).json({
            success : true,
            result:  response,
            message: 'Collection Found Successfully'
          })
    }
  } catch (error) {
    handleError(res, error)
  }
}

const getallCollection = async (req, res) => {
  try {
    
    // console.log(req.user.Wallet_Address,'wallet iis ');
console.log(req.user)
    const wallet = req.user.Wallet_Address
    // console.log(wallet,'walletsss');
  
    const response = await Collection.find({wallet_Id : { $not: new RegExp(wallet, 'i') }})
console.log(response)
    if(response.length <= 0){
        res.status(400).json({
          success : false,
            result:  "",
            message: 'No Collection Found'}
        )
    }else{
        res.status(200).json({
            success : true,
            result:  response,
            message: 'Collection Found Successfully'
          })
    }
  } catch (error) {
    handleError(res, error)
  }
}


module.exports = { getCollection,getallCollection }
