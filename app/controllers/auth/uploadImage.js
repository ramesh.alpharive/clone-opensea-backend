/* eslint-disable linebreak-style */
/* eslint-disable prettier/prettier */
const User = require('../../models/user') 

const { handleError } = require('../../middleware/utils')

/**
 * Update item function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const uploadImage = async (req, res) => {
//    console.log('qwfredfedgehg');
    try {
        if (req.file) {

            const data = req.file.filename
            const response = `${process.env.BACKEND_URL}/images/${data}`

            res.status(200).json({
                success: true,
                result: response,
                message: 'IMAGE UPLOADED SUCCESSFULLY'
            })
        } else {
            res.status(400).json({
                success: false,
                result: null,
                message: 'SOMETHING WENT WRONG'
            })
        }
    } catch (error) {
        handleError(res, error)
    }
}

module.exports = { uploadImage }
