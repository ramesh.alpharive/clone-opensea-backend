const { forgotPassword } = require('./forgotPassword')
const { getRefreshToken } = require('./getRefreshToken')
const { createCollection } = require('./createCollection')
const { loginByAdmin } = require('./loginByAdmin')
const { register } = require('./register')
const { resetPassword } = require('./resetPassword')
const { roleAuthorization } = require('./roleAuthorization')
const { verify } = require('./verify')
const { getCollection,getallCollection } = require('./getCollection')
const { createUser } = require('./createUser')
const { uploadImage } = require('./uploadImage')
const { getNftByCollectionid } = require('./getNftByCollectionid')
const { getCollectionDetails } = require('./getCollectionDetails')
const { getMyCollection } = require('./getMyCollection')


module.exports = {
  forgotPassword,
  getRefreshToken,
  createCollection,
  loginByAdmin,
  register,
  getallCollection,
  resetPassword,
  roleAuthorization,
  verify,
  getCollection,
  createUser,
  uploadImage,
  getNftByCollectionid,
  getCollectionDetails,
  getMyCollection
}
