const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const Collection = require('../../models/collections')


const getMyCollection = async (req, res) => {
    try {
      
      
//   console.log(req.user,"found my collection")
      const wallet = req.user.Wallet_Address
    
      const response = await Collection.find({wallet_Id : wallet })
  console.log(response)
      if(response.length <= 0){
          res.status(400).json({
            success : false,
              result:  "",
              message: 'No Collection Found'}
          )
      }else{
          res.status(200).json({
              success : true,
              result:  response,
              message: 'Collection Found Successfully'
            })
      }
    } catch (error) {
      handleError(res, error)
    }
  }   


  module.exports = { getMyCollection }
