const { validateResult } = require('../../../middleware/utils')
const { check } = require('express-validator')

/**
 * Validates forgot password request
 */
const validateCreateUser = [
  // check('name')
  //   .exists()
  //   .withMessage('Name Missing')
  //   .not()
  //   .isEmpty()
  //   .withMessage('Name Is Empty'),

    check('Wallet_Address')
    .exists()
    .withMessage('Wallet Address Missing')
    .not()
    .isEmpty()
    .withMessage('Wallet Address Is Empty'),
    
    // check('Image')
    // .exists()
    // .withMessage('Image Missing')
    // .not()
    // .isEmpty()
    // .withMessage('Image Is Empty'),
  
  (req, res, next) => {
    validateResult(req, res, next)
  }
]

module.exports = { validateCreateUser }
