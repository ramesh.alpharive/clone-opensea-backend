const { validateForgotPassword } = require('./validateForgotPassword')
const { validateCreateCollection } = require('./validateCreateCollection')
const { validateRegister } = require('./validateRegister')
const { validateResetPassword } = require('./validateResetPassword')
const { validateVerify } = require('./validateVerify')
const { validateCreateUser } = require('./validateCreateUser')
const { validategetCollectionDetails } = require('./validategetCollectionDetails')

module.exports = {
  validateForgotPassword,
  validateCreateCollection,
  validateRegister,
  validateResetPassword,
  validateVerify,
  validateCreateUser,
  validategetCollectionDetails

}
