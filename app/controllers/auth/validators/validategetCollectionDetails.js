const { validateResult } = require('../../../middleware/utils')
const { check } = require('express-validator')

/**
 * Validates getCollectionDetails request
 */
const validategetCollectionDetails = [

    check('collection_id')
    .exists()
    .withMessage('Collection_id Missing')
    .not()
    .isEmpty()
    .withMessage('Collection_id Is Empty'),
    

  (req, res, next) => {
    validateResult(req, res, next)
  }
]

module.exports = { validategetCollectionDetails }
