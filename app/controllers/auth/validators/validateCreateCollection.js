const { validateResult } = require('../../../middleware/utils')
const { check } = require('express-validator')

/**
 * Validates register request
 */
/**
 * Validates login request
 */
const validateCreateCollection = [
  check('Collection_Name')
    .exists()
    .withMessage('Collection_Name Missing'),
  

  check('wallet_Id')
    .exists()
    .withMessage('Collection_Type Missing')
    .not()
    .isEmpty()
    .withMessage('Collection_Type is_empty'),

    check('Logo_Image')
    .exists()
    .withMessage('Collection_URL Missing')
    .not()
    .isEmpty()
    .withMessage('Collection_URL is_empty'),

    check('Network')
    .exists()
    .withMessage('Network_Type Missing')
    .not()
    .isEmpty()
    .withMessage('Network_Type is_empty'),

  
    check('symbol')
    .exists()
    .withMessage('Bio Missing')
    .not()
    .isEmpty()
    .withMessage('Bio is_empty'),

    // check('wallet_Id')
    // .exists()
    // .withMessage('wallet_Id Missing')
    // .not()
    // .isEmpty()
    // .withMessage('wallet_Id is_empty'),

    // check('Symbol')
    // .exists()
    // .withMessage('wallet_Id Missing')
    // .not()
    // .isEmpty()
    // .withMessage('wallet_Id is_empty'),

    // check('Chain_ID')
    // .exists()
    // .withMessage('wallet_Id Missing')
    // .not()
    // .isEmpty()
    // .withMessage('wallet_Id is_empty'),
  (req, res, next) => {
    validateResult(req, res, next)
  }
]

module.exports = { validateCreateCollection }
