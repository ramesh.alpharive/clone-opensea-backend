const { matchedData } = require('express-validator')
const { handleError } = require('../../middleware/utils')
const Collection = require('../../models/collections')
const User = require('../../models/user')

/**
 * Login function called by route
 * @param {Object} req - request object
 * @param {Object} res - response object
 */
const createCollection = async (req, res) => {
  try {
    const { wallet_Id, Collection_Name, Logo_Image, Network, symbol } = req.body

    const collection = new Collection({
    
     wallet_Id,
     Collection_Name,
     Logo_Image,
     Network,
     symbol

   })
   collection.save((err) => {
     if (err) {
      res.status(400).json(err)         
      }
   })

   const id = collection._id
   const response = await User.findOneAndUpdate({Wallet_Address : wallet_Id },{$push : {collections : id}})
   
   res.status(201).json({
    success : true,
    result:  collection,
    message: 'Collection Created Successfully'
  })
  } catch (error) {
    handleError(res, error)
  }
}

module.exports = { createCollection }
